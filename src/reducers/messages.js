import {
  FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_FAILURE,
} from '../constants';

const initialState = {
  isFetching: false,
  datas: {},
  errorMsg: '',
};

export default function messages(state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_MESSAGE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        datas: Object.assign({}, state.datas, action.messages),
      });

    case FETCH_MESSAGE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMsg: action.errorMsg,
      });

    default:
      return state;
  }
}
