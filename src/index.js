/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLoger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers';
import App from './App';
import ChatList from './components/ChatList';
import ChatPage from './pages/ChatPage';
import Login from './components/Login';
import PageNotFound from './components/404';

const loggerMiddleware = createLoger();

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
  )),
);

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={ChatList} />
        <Route path="chat/:id" component={ChatPage} />
        <Route path="login" component={Login} />
        <Route path="*" component={PageNotFound} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
