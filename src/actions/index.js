import fetch from 'isomorphic-fetch';
import {
  FETCH_MESSAGE_REQUEST, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_FAILURE,
} from '../constants';

// fetch message
// sync action: request
function fetchMessageRequest() {
  return {
    type: FETCH_MESSAGE_REQUEST,
    isFetching: true,
  };
}

// sync action: success
function fetchMessageSuccess(messages) {
  return {
    type: FETCH_MESSAGE_SUCCESS,
    isFetching: false,
    messages,
  };
}

// sync action: failure
function fetchMessageFailure(errorMsg) {
  return {
    type: FETCH_MESSAGE_FAILURE,
    isFetching: false,
    errorMsg,
  };
}

// handle 500/404 error
function handleErorrs(response) {
  if (!response.ok) throw new Error(response.statusText);
  return response;
}

// async action: fetch message
export default function fetchMessages(recipientID) {
  return (dispatch) => {
    dispatch(fetchMessageRequest());
    return fetch(`http://localhost:1234/messages/${recipientID}`)
      .then(handleErorrs)
      .then(response => response.json())
      .then(json => dispatch(fetchMessageSuccess(json)))
      .catch(error => dispatch(fetchMessageFailure(error)));
  };
}
