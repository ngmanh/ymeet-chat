import React from 'react';
import FaSmileO from 'react-icons/lib/fa/smile-o';
import FaHeart from 'react-icons/lib/fa/heart';
import FaPaperPlane from 'react-icons/lib/fa/paper-plane';

const MessageInput = () => (
  <form className="ymc-form" action="/">
    <div className="ymc-form__group">
      <input
        type="text"
        className="ymc-form__input"
        placeholder="Write a message..."
        rows="1"
      />
      <button className="ymc-form__send-btn">
        <FaPaperPlane />
      </button>
    </div>
    <button className="ymc-form__emoji">
      <FaSmileO size={24} />
    </button>
    <button className="ymc-form__cta-btn">
      <FaHeart size={24} />
    </button>
  </form>
);

// class MessageInput extends Component {
//   render() {
//     return (
//       <form className="ymc-form" action="/">
//         <div className="ymc-form__group">
//           <input
//             type="text"
//             className="ymc-form__input"
//             placeholder="Write a message..."
//             rows="1"
//           />
//           <button className="ymc-form__send-btn">
//             <FaPaperPlane />
//           </button>
//         </div>
//         <button className="ymc-form__emoji">
//           <FaSmileO size={24} />
//         </button>
//         <button className="ymc-form__cta-btn">
//           <FaHeart size={24} />
//         </button>
//       </form>
//     );
//   }
// }

export default MessageInput;
