import React from 'react';

// because of lazy so loader's width is inline style
const shortMsgStyle = {
  width: 100,
};

const mediumMsgStyle = {
  width: 180,
};

const LoaderMessages = () => (
  <div>
    <div className="ymc-dialogue">
      <div className="ymc-dialogue__chathead">
        <div className="chathead">
          <div className="img-ghost"></div>
        </div>
      </div>
      <div className="ymc-dialogue__content">
        <div className="ymc-messages loader">
          <div className="ymc-messages__content">
            <div className="ymc-messages__msg">
              <div className="ymc-messages__msg__content" style={mediumMsgStyle}></div>
            </div>
            <div className="ymc-messages__msg">
              <div className="ymc-messages__msg__content"></div>
            </div>
            <div className="ymc-messages__msg">
              <div className="ymc-messages__msg__content" style={shortMsgStyle}></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="ymc-dialogue ymc-dialogue--2 loader">
      <div className="ymc-dialogue__content">
        <div className="ymc-messages loader">
          <div className="ymc-messages__content ymc-messages__content--2">
            <div className="ymc-messages__msg">
              <div className="ymc-messages__msg__content" style={shortMsgStyle}></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default LoaderMessages;
