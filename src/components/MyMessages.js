import React, { PropTypes } from 'react';
import MyMessage from './messages/MyMessage';

const MyMessages = ({ content }) => (
  <div className="ymc-messages">
    <div className="ymc-messages__content ymc-messages__content--2">
      <MyMessage text={content} />
    </div>
  </div>
);

MyMessages.propTypes = {
  content: PropTypes.string,
};

export default MyMessages;
