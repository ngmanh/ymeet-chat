import React, { PropTypes } from 'react';
import Message from './messages/Message';

const Messages = ({ content }) => (
  <div className="ymc-messages">
    <div className="ymc-messages__content">
      <Message text={content} />
    </div>
  </div>
);

Messages.propTypes = {
  content: PropTypes.string,
};

export default Messages;
