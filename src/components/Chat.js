import React, { Component, PropTypes } from 'react';
import AppTopBar from './AppTopBar';
import ChatFrame from './ChatFrame';
import AppBottomBar from './AppBottomBar';

class Chat extends Component {
  componentDidMount() {
    this.props.fetchMessages(this.props.params.id);
  }

  render() {
    const props = this.props;

    return (
      <div className="yc-container">
        <AppTopBar />
        <ChatFrame {...props} />
        <AppBottomBar />
      </div>
    );
  }
}

Chat.propTypes = {
  fetchMessages: PropTypes.func,
  params: PropTypes.object,
};

export default Chat;
