import React from 'react';

const DialogueChathead = () => (
  <div className="ymc-dialogue__chathead">
    <div className="chathead">
      <img src="https://s3.amazonaws.com/uifaces/faces/twitter/jina/128.jpg" role="presentation" />
    </div>
  </div>
);

export default DialogueChathead;
