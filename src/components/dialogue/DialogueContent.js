import React, { PropTypes } from 'react';
import Messages from '../Messages';
import MyMessages from '../MyMessages';

const DialogueContent = props => (
  <div className="ymc-dialogue__content">
    {props.from_recipient ? (
      <Messages {...props} />
    ) : (
      <MyMessages {...props} />
    )}
  </div>
);

DialogueContent.propTypes = {
  from_recipient: PropTypes.bool,
};

export default DialogueContent;
