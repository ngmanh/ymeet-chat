import React from 'react';

const AppTopBar = () => (
  <div>
    <header className="ymc-topbar">
      <div>Chat Top Bar</div>
    </header>
    <div className="ymc-topbar__ghost"></div>
  </div>
);

export default AppTopBar;
