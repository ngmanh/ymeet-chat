import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import Dialogue from './Dialogue';
import MyDialogue from './MyDialogue';
import Divider from './Divider';
import LoaderMessages from './loaders/LoaderMessages';
// import Typing from './Typing';

class ChatFrame extends Component {
  constructor() {
    super();
    this.renderAllDialogue = this.renderAllDialogue.bind(this);
  }

  renderAllDialogue(datas) {
    return datas.messages.map((m, index) => {
      if (m.from_recipient) {
        return (<Dialogue key={index} {...m} />);
      }

      return (<MyDialogue key={index} {...m} />);
    });
  }

  render() {
    const datas = this.props.messages.datas;
    const isShowLoader = isEmpty(datas);

    return (
      <section className="ymc-c-f">

        <Divider text="30 Thg 6, 2016" />

        {isShowLoader ? (
          <LoaderMessages />
        ) : this.renderAllDialogue(datas)}

        {/* <Typing /> */}

      </section>
    );
  }
}

ChatFrame.propTypes = {
  messages: PropTypes.object,
};

export default ChatFrame;
