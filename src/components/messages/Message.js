import React, { PropTypes } from 'react';

const Message = props => (
  <div className="ymc-messages__msg">
    <div className="ymc-messages__msg__content">{props.text}</div>
  </div>
);

Message.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Message;
