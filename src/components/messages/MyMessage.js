import React, { PropTypes } from 'react';

const MyMessage = props => (
  <div className="ymc-messages__msg">
    <div className="ymc-messages__msg__content">{props.text}</div>
    <div className="ymc-messages__meta">
      <img
        className="ymc-messages__meta__img"
        src="https://s3.amazonaws.com/uifaces/faces/twitter/jina/128.jpg"
        role="presentation"
      />
    </div>
  </div>
);

MyMessage.propTypes = {
  text: PropTypes.string,
};

export default MyMessage;

// old message
/* <div className="ymc-messages__msg">
  <div className="ymc-messages__msg__content">mang theo đồ ăn cho khách</div>
  <div className="ymc-messages__meta"></div>
</div> */

// sent
/* <div className="ymc-messages__msg">
  <div
    className="ymc-messages__msg__content"
  >lấy hết đồ ăn ra khỏi bồn, đèn sẽ tắt và khách có thể thưởng thức món ăn.</div>
  <div className="ymc-messages__meta"><i className="fa fa-check-circle-o"></i></div>
</div> */

// received
/* <div className="ymc-messages__msg">
  <div className="ymc-messages__msg__content">Thực khách</div>
  <div className="ymc-messages__meta"><i className="fa fa-check-circle"></i></div>
</div> */
