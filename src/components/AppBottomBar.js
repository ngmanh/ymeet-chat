import React from 'react';
import MessageInput from './MessageInput';

const AppBottomBar = () => (
  <div>
    <section className="ymc-bottombar">
      <MessageInput />
    </section>
    <div className="ymc-bottombar__ghost"></div>
  </div>
);

export default AppBottomBar;
