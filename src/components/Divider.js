import React, { PropTypes } from 'react';

const Divider = props => (
  <div className="ymc-hr">
    <div className="ymc-hr__divide"></div>
    <div className="ymc-hr__text">{props.text}</div>
    <div className="ymc-hr__divide"></div>
  </div>
);

Divider.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Divider;
