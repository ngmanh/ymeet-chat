import React from 'react';
import DialogueContent from './dialogue/DialogueContent';

const MyDialogue = props => (
  <div className="ymc-dialogue ymc-dialogue--2">
    <DialogueContent {...props} />
  </div>
);

export default MyDialogue;
