import React from 'react';
import DialogueChathead from './dialogue/DialogueChathead';
import DialogueContent from './dialogue/DialogueContent';

const Dialouge = props => (
  <div className="ymc-dialogue">
    <DialogueChathead {...props} />
    <DialogueContent {...props} />
  </div>
);

export default Dialouge;
