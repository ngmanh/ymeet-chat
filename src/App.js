import React, { PropTypes } from 'react';
import './_assets/main.css';

const App = props => (
  <div>
    {props.children}
  </div>
);

App.propTypes = {
  children: PropTypes.node.isRequired,
};

export default App;
