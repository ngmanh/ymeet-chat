import { connect } from 'react-redux';
import fetchMessages from '../actions';
import Chat from '../components/Chat';

function mapStateToProps(state) {
  return state;
}

function mapDispatchToProps(dispatch) {
  return {
    fetchMessages: recipientID => dispatch(fetchMessages(recipientID)),
  };
}

const ChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);

export default ChatContainer;
