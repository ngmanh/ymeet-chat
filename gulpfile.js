const gulp = require('gulp');
const eslint = require('gulp-eslint');

gulp.task('eslint', () => {
  gulp.src('src/index.js')
    .pipe(eslint({
      useEslintrc: true
    }))
    .pipe(eslint.format());
});

gulp.task('watch', ['eslint'], () => {
  gulp.watch('src/**/*.js', ['eslint']);
});
